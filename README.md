# Construyendo sistemas de aprendizaje de máquina con Python


## Contenido
1. [Capítulo 1: Comenzando con Python Machine Learning](/Capítulo_1_Comenzando_con_Python_Machine_Learning)
2. [Capítulo 2: Aprendiendo a clasificar con ejemplos del mundo real](/Capítulo_2_Aprendiendo_a_Clasificar)
3. [Capítulo 3: Clustering - Encontrando publicaciones relacionadas](/Capítulo_3_Clustering)
4. [Capítulo 4: Modelado de temas](Capítulo_4_Modelado de temas)
5. [Capítulo 5: Clasificación 1 - Detección de respuestas pobres](/Capítulo_5_Clasificacion_1)
6. [Capítulo 6: Clasificación 2 - Análisis de Sentimientos](/Capítulo_6_Clasificacion_2)


## Gitbook

[Ver](https://luis.pinedo22.gitlab.io/construyendo-sistemas-de-aprendizaje-de-maquina-con-python)


## Licencia

Bajo licencia de MIT - [Ver licencia](/LICENSE)
