# Summary

* [Capítulo 1: Comenzando con Python Machine Learning](/Capítulo_1_Comenzando_con_Python_Machine_Learning/README.md)
* [Capítulo 2: Aprendiendo a clasificar con ejemplos del mundo real](/Capítulo_2_Aprendiendo_a_Clasificar/README.md)
* [Capítulo 3: Clustering - Encontrando publicaciones relacionadas](/Capítulo_3_Clustering/README.md)
* [Capítulo 4: Modelado de temas](/Capítulo_4_Modelado de temas/README.md)
* [Capítulo 5: Clasificación 1 - Detección de respuestas pobres](/Capítulo_5_Clasificacion_1/README.md)
* [Capítulo 6: Clasificación 2 - Análisis de Sentimientos](/Capítulo_6_Clasificacion_2/README.md)
