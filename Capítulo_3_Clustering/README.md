# Capítulo 3: Clustering - Encontrando publicaciones relacionadas


Llamamos a este aprendizaje supervisado, ya que el aprendizaje fue guiado por un maestro; En nuestro caso el profesor tuvo la forma de clasificaciones correctas.


## Medición de la relación de las publicaciones

Desde el punto de vista del aprendizaje automático, el texto sin formato no sirve para nada. Solo si logramos transformarlo en números significativos, podemos introducirlo en nuestro aprendizaje automático.
Algoritmos como el agrupamiento. Lo mismo es cierto para las operaciones más mundanas en el texto, como la medición de similitud.


## Preprocesamiento - similitud medida como un número similar de palabras comunes

Como hemos visto anteriormente, el enfoque de la bolsa de palabras es rápido y robusto. Sin embargo, no está exenta de desafíos.

### Convertir texto sin formato en una bolsa de palabras

Las funciones y clases de Scikit se importan a través del paquete sklearn de la siguiente manera:

```python
from sklearn.feature_extraction.text import CountVectorizer
vectorizer = CountVectorizer(min_df=1)
```

```python
import os
import sys
import scipy as sp
from sklearn.feature_extraction.text import CountVectorizer

DATA_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "data")

if not os.path.exists(DATA_DIR):
    print("Uh, esperábamos un directorio de datos, que contiene los datos del juguete")
    sys.exit(1)

CHART_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "charts")
if not os.path.exists(CHART_DIR):
    os.mkdir(CHART_DIR)
    

TOY_DIR = os.path.join(DATA_DIR, "toy")
posts = [open(os.path.join(TOY_DIR, f)).read() for f in os.listdir(TOY_DIR)]

new_post = "imaging databases"

import nltk.stem
english_stemmer = nltk.stem.SnowballStemmer('english')


class StemmedCountVectorizer(CountVectorizer):

    def build_analyzer(self):
        analyzer = super(StemmedCountVectorizer, self).build_analyzer()
        return lambda doc: (english_stemmer.stem(w) for w in analyzer(doc))

vectorizer = StemmedCountVectorizer(min_df=1, stop_words='english')

from sklearn.feature_extraction.text import TfidfVectorizer


class StemmedTfidfVectorizer(TfidfVectorizer):

    def build_analyzer(self):
        analyzer = super(StemmedTfidfVectorizer, self).build_analyzer()
        return lambda doc: (english_stemmer.stem(w) for w in analyzer(doc))

vectorizer = StemmedTfidfVectorizer(
    min_df=1, stop_words='english', decode_error='ignore')

X_train = vectorizer.fit_transform(posts)

num_samples, num_features = X_train.shape
print("#samples: %d, #features: %d" % (num_samples, num_features))

new_post_vec = vectorizer.transform([new_post])
print(new_post_vec, type(new_post_vec))
print(new_post_vec.toarray())
print(vectorizer.get_feature_names())


def dist_raw(v1, v2):
    delta = v1 - v2
    return sp.linalg.norm(delta.toarray())


def dist_norm(v1, v2):
    v1_normalized = v1 / sp.linalg.norm(v1.toarray())
    v2_normalized = v2 / sp.linalg.norm(v2.toarray())

    delta = v1_normalized - v2_normalized

    return sp.linalg.norm(delta.toarray())

dist = dist_norm

best_dist = sys.maxsize
best_i = None

for i in range(0, num_samples):
    post = posts[i]
    if post == new_post:
        continue
    post_vec = X_train.getrow(i)
    d = dist(post_vec, new_post_vec)

    print("=== Publicación %i con dist=%.2f: %s" % (i, d, post))

    if d < best_dist:
        best_dist = d
        best_i = i

print("La mejor publicación es %i with dist=%.2f" % (best_i, best_dist))
```


Tenemos nuestra primera medida de similitud. La publicación 0 es la más diferente de nuestra nueva publicación. Comprensiblemente, no tiene una sola palabra en común con la nueva publicación. También podemos entender que la publicación 1 es muy similar a la publicación nueva, pero no al ganador, ya que contiene una palabra más que la publicación 3 que no está incluida en la publicación nueva.
En cuanto a los mensajes 3 y 4, sin embargo, la imagen ya no es tan clara. La publicación 4 es la misma que la publicación 3, duplicada tres veces. Por lo tanto, también debería ser de la misma similitud con la nueva publicación que la publicación 3.


### Normalizando los vectores de conteo de palabras

Tendremos que extender dist_raw para calcular la distancia del vector, no en los vectores en bruto sino en los normalizados:


```python
def dist_norm(v1, v2):
	v1_normalized = v1/sp.linalg.norm(v1.toarray())
	v2_normalized = v2/sp.linalg.norm(v2.toarray())
	delta = v1_normalized - v2_normalized
	return sp.linalg.norm(delta.toarray())
```

### Eliminar palabras menos importantes

Vamos a echar otro vistazo al Post 2. De sus palabras que no están en el nuevo post, tenemos "más", "seguro", "imágenes" y "permanentemente". En realidad, son bastante diferentes en la importancia general de la publicación. Las palabras como "la mayoría" aparecen muy a menudo en todo tipo de contextos diferentes, y las palabras como esta se llaman palabras para detener. No llevan tanta información y, por lo tanto, no se deben pesar tanto como palabras como "imágenes", que no ocurren a menudo en contextos diferentes. La mejor opción sería eliminar todas las palabras que son tan frecuentes que no ayudan a distinguir entre diferentes textos. Estas palabras se llaman palabras de alto.

Como este es un paso tan común en el procesamiento de texto, hay un parámetro simple en CountVectorizer para lograr esto, de la siguiente manera:

```python
vectorizer = CountVectorizer(min_df=1, stop_words='english')
sorted(vectorizer.get_stop_words())[0:20]
```


#### Instalación y uso de NLTK

Básicamente, deberá instalar los dos paquetes NLTK y PyYAML.

Para verificar si su instalación fue exitosa, abra un intérprete de Python y escriba lo siguiente:


```python
>>> import nltk
```

NLTK viene con diferentes stemmers. Esto es necesario, porque cada idioma tiene un conjunto diferente de reglas para la derivación. Para el inglés, podemos tomar SnowballStemmer.

```python
>>> import nltk.stem
>>> s= nltk.stem.SnowballStemmer('english')
>>> s.stem("graphics")
u'graphic'
>>> s.stem("imaging")
u'imag'
>>> s.stem("image")
u'imag'
>>> s.stem("imagination")
u'imagin'
>>> s.stem("imagine")
u'imagin'
```

También funciona con verbos de la siguiente manera:

```python
>>> s.stem("buys")
u'buy'
>>> s.stem("buying")
u'buy'
>>> s.stem("bought")
u'bought'
```

#### Extendiendo el vectorizador con stemmer de NLTK

Necesitamos detener los mensajes antes de incluirlos en CountVectorizer. La clase proporciona varios enlaces con los que podríamos personalizar las etapas de preprocesamiento y tokenización. El preprocesador y el tokenizador se pueden configurar en el constructor como parámetros. No queremos colocar el modulador en ninguno de ellos, porque tendríamos que hacer la tokenización y la normalización por nosotros mismos. En su lugar, sobrescribimos el método build_analyzer de la siguiente manera:

```python
>>> import nltk.stem
>>> english_stemmer = nltk.stem.SnowballStemmer('english')
>>> class StemmedCountVectorizer(CountVectorizer):
...	def build_analyzer(self):
...	analyzer = super(StemmedCountVectorizer, self).build_ analyzer()
...	return lambda doc: (english_stemmer.stem(w) for w in analyzer(doc))
>>> vectorizer = StemmedCountVectorizer(min_df=1, stop_ words='english')
```


Como resultado, ahora tenemos una característica menos, porque "images" y "imaging" se colapsaron en una. El conjunto de nombres de características se parece a lo siguiente:

```
u'actual', u'capabl', u'contain', u'data', u'databas', u'imag',
u'interest', u'learn', u'machin', u'perman', u'post', u'provid',
u'safe', u'storag', u'store', u'stuff', u'toy']
```



### Detener las palabras en los esteroides

Ahora que tenemos una manera razonable de extraer un vector compacto de una publicación textual ruidosa, retrocedamos un momento para pensar qué significan realmente los valores de las características.

Los valores de las características simplemente cuentan las ocurrencias de términos en una publicación. Suponemos en silencio que los valores más altos para un término también significan que el término es de mayor importancia para el puesto dado. Pero ¿qué pasa, por ejemplo, con la palabra "subject", que ocurre naturalmente en cada una de las publicaciones? Muy bien, podríamos decirle a CountVectorizer que lo elimine también por medio de su parámetro max_df. Podríamos, por ejemplo, establecerlo en 0.9 para que todas las palabras que aparecen en más del 90 por ciento de todas las publicaciones siempre sean ignoradas. Pero ¿qué pasa con las palabras que aparecen en el 89 por ciento de todas las publicaciones? ¿Qué tan bajo estaríamos dispuestos a establecer max_df? El problema es que como sea que lo configuremos, siempre habrá el problema de que algunos términos son más discriminatorios que otros.


Esto solo se puede resolver contando las frecuencias de los términos para cada publicación y, además, descontando las que aparecen en muchas publicaciones. En otras palabras, queremos un alto valor para un término dado en un valor dado si ese término aparece a menudo en ese puesto en particular y muy raramente en cualquier otro lugar.



Esto es exactamente lo que hace el término frecuencia: frecuencia de documentos inversos (TF-IDF); TF representa la parte de conteo, mientras que las FDI toman en cuenta el descuento. Una implementación ingenua se vería como la siguiente:


```python
import scipy as sp


def tfidf(t, d, D):
    tf = float(d.count(t)) / sum(d.count(w) for w in set(d))
    idf = sp.log(float(len(D)) / (len([doc for doc in D if t in doc])))
    return tf * idf


a, abb, abc = ["a"], ["a", "b", "b"], ["a", "b", "c"]
D = [a, abb, abc]

print(tfidf("a", a, D))
print(tfidf("b", abb, D))
print(tfidf("a", abc, D))
print(tfidf("b", abc, D))
print(tfidf("c", abc, D))
```



## Clustering

La agrupación plana divide las publicaciones en un conjunto de agrupaciones sin relacionar las agrupaciones entre sí. El objetivo es simplemente crear una partición tal que todas las publicaciones en un grupo sean las más similares entre sí y se diferencien de las publicaciones en todos los demás grupos. Muchos algoritmos de agrupación plana requieren que la cantidad de agrupaciones se especifique por adelantado.


Scikit proporciona una amplia gama de enfoques de agrupamiento en el paquete sklearn.


### KMeans

KMeans es el algoritmo de agrupamiento plano más utilizado. Después de que se inicializa con el número deseado de agrupaciones, num_clusters, mantiene esa cantidad de los llamados centroides agrupados. Inicialmente, seleccionaría cualquiera de las publicaciones num_clusters y establecería los centroides a su vector de características. Luego pasaría por todas las demás publicaciones y les asignaría el centroide más cercano como su grupo actual. Luego moverá cada centroide al centro de todos los vectores de esa clase en particular. Esto cambia, por supuesto, la asignación de cluster. Algunas publicaciones están ahora más cerca de otro grupo. Por lo tanto, se actualizarán las asignaciones para los mensajes modificados. Esto se hace siempre que los centroides se muevan una cantidad considerable. Después de algunas iteraciones, los movimientos caerán por debajo de un umbral y consideramos que la agrupación está convergida.




```python
import os
import scipy as sp
from scipy.stats import norm
from matplotlib import pylab
from sklearn.cluster import KMeans

DATA_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "data")

if not os.path.exists(DATA_DIR):
    print("Uh, esperábamos un directorio de datos, que contiene los datos del juguete")
    sys.exit(1)

CHART_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "charts")
if not os.path.exists(CHART_DIR):
    os.mkdir(CHART_DIR)

seed = 2
sp.random.seed(seed)  

num_clusters = 3


def plot_clustering(x, y, title, mx=None, ymax=None, xmin=None, km=None):
    pylab.figure(num=None, figsize=(8, 6))
    if km:
        pylab.scatter(x, y, s=50, c=km.predict(list(zip(x, y))))
    else:
        pylab.scatter(x, y, s=50)

    pylab.title(title)
    pylab.xlabel("Occurrence word 1")
    pylab.ylabel("Occurrence word 2")

    pylab.autoscale(tight=True)
    pylab.ylim(ymin=0, ymax=1)
    pylab.xlim(xmin=0, xmax=1)
    pylab.grid(True, linestyle='-', color='0.75')

    return pylab


xw1 = norm(loc=0.3, scale=.15).rvs(20)
yw1 = norm(loc=0.3, scale=.15).rvs(20)

xw2 = norm(loc=0.7, scale=.15).rvs(20)
yw2 = norm(loc=0.7, scale=.15).rvs(20)

xw3 = norm(loc=0.2, scale=.15).rvs(20)
yw3 = norm(loc=0.8, scale=.15).rvs(20)

x = sp.append(sp.append(xw1, xw2), xw3)
y = sp.append(sp.append(yw1, yw2), yw3)

i = 1
plot_clustering(x, y, "Vectors")
pylab.savefig(os.path.join("../img/image1.png" % i))
pylab.clf()

i += 1



mx, my = sp.meshgrid(sp.arange(0, 1, 0.001), sp.arange(0, 1, 0.001))

km = KMeans(init='random', n_clusters=num_clusters, verbose=1,
            n_init=1, max_iter=1,
            random_state=seed)
km.fit(sp.array(list(zip(x, y))))

Z = km.predict(sp.c_[mx.ravel(), my.ravel()]).reshape(mx.shape)

plot_clustering(x, y, "Clustering iteration 1", km=km)
pylab.imshow(Z, interpolation='nearest',
             extent=(mx.min(), mx.max(), my.min(), my.max()),
             cmap=pylab.cm.Blues,
             aspect='auto', origin='lower')

c1a, c1b, c1c = km.cluster_centers_
pylab.scatter(km.cluster_centers_[:, 0], km.cluster_centers_[:, 1],
              marker='x', linewidth=2, s=100, color='black')
pylab.savefig(os.path.join("../img/image2.png" % i))
pylab.clf()

i += 1

km = KMeans(init='random', n_clusters=num_clusters, verbose=1,
            n_init=1, max_iter=2,
            random_state=seed)
km.fit(sp.array(list(zip(x, y))))

Z = km.predict(sp.c_[mx.ravel(), my.ravel()]).reshape(mx.shape)

plot_clustering(x, y, "Clustering iteration 2", km=km)
pylab.imshow(Z, interpolation='nearest',
             extent=(mx.min(), mx.max(), my.min(), my.max()),
             cmap=pylab.cm.Blues,
             aspect='auto', origin='lower')

c2a, c2b, c2c = km.cluster_centers_
pylab.scatter(km.cluster_centers_[:, 0], km.cluster_centers_[:, 1],
              marker='x', linewidth=2, s=100, color='black')

pylab.gca().add_patch(
    pylab.Arrow(c1a[0], c1a[1], c2a[0] - c1a[0], c2a[1] - c1a[1], width=0.1))
pylab.gca().add_patch(
    pylab.Arrow(c1b[0], c1b[1], c2b[0] - c1b[0], c2b[1] - c1b[1], width=0.1))
pylab.gca().add_patch(
    pylab.Arrow(c1c[0], c1c[1], c2c[0] - c1c[0], c2c[1] - c1c[1], width=0.1))

pylab.savefig(os.path.join("../img/image3.png" % i))
pylab.clf()

i += 1


km = KMeans(init='random', n_clusters=num_clusters, verbose=1,
            n_init=1, max_iter=10,
            random_state=seed)
km.fit(sp.array(list(zip(x, y))))

Z = km.predict(sp.c_[mx.ravel(), my.ravel()]).reshape(mx.shape)

plot_clustering(x, y, "Clustering iteration 10", km=km)
pylab.imshow(Z, interpolation='nearest',
             extent=(mx.min(), mx.max(), my.min(), my.max()),
             cmap=pylab.cm.Blues,
             aspect='auto', origin='lower')

pylab.scatter(km.cluster_centers_[:, 0], km.cluster_centers_[:, 1],
              marker='x', linewidth=2, s=100, color='black')
pylab.savefig(os.path.join("../img/image4.png" % i))
pylab.clf()

i += 1
```


Juguemos esto con un ejemplo de publicaciones de juguete que contengan solo dos palabras. Cada punto en el siguiente cuadro representa un documento:

![](img/image1.png)


Después de ejecutar una iteración de KMeans, es decir, tomar dos vectores como puntos de inicio, asignar etiquetas al resto y actualizar los centros del clúster para que sean el nuevo punto central de todos los puntos en ese clúster, obtenemos la siguiente agrupación:

![](img/image2.png)


Debido a que los centros de clúster se mueven, tenemos que reasignar las etiquetas de clúster y volver a calcular los centros de clúster. Después de la iteración 2, obtenemos el siguiente agrupamiento:

![](img/image3.png)


Las flechas muestran los movimientos de los centros del racimo. Después de cinco iteraciones en este ejemplo, los centros del clúster ya no se mueven notablemente (el umbral de tolerancia de Scikit es 0,0001 por defecto).

Una vez que la agrupación se ha establecido, solo debemos anotar los centros de agrupación y su identidad. Cuando llega cada nuevo documento, tenemos que vectorizarlo y compararlo con todos los centros de cluster. El centro del clúster con la menor distancia a nuestro nuevo vector de publicaciones pertenece al clúster que asignaremos a la nueva publicación.


