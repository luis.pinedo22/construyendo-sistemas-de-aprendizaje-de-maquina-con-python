# Capítulo 4: Modelado de temas

En este capítulo, aprenderemos métodos que no agrupan objetos, sino que los colocamos en una pequeña cantidad de grupos llamados temas. También aprenderemos cómo derivar entre los temas que son fundamentales para el texto y otros que se mencionan de forma vaga (este libro menciona el trazar de vez en cuando, pero no es un tema central, como lo es el aprendizaje automático). El subcampo de aprendizaje automático que trata estos problemas se denomina modelado de temas.


## Asignación de Dirichlet latente (LDA)

LDA y LDA: desafortunadamente, hay dos métodos en el aprendizaje automático con las iniciales LDA: asignación de Dirichlet latente, que es un método de modelado de temas; y el análisis discriminante lineal, que es un método de clasificación. No tienen ninguna relación, excepto por el hecho de que las iniciales LDA pueden referirse a cualquiera. Sin embargo, esto puede ser confuso. Scikit-learn tiene un submódulo, sklearn.lda, que implementa un análisis discriminante lineal. Por el momento, scikit-learn no implementa la asignación de Dirichlet latente.



### Building a topic model

Desafortunadamente, scikit-learn no admite la asignación de Dirichlet latente. Por lo tanto, vamos a utilizar el paquete gensim en Python. Gensim es desarrollado por Radim Řehůřek, que es un investigador y consultor de aprendizaje automático en la República Checa. Hay que empezar por instalarlo. Podemos lograr esto ejecutando uno de los siguientes comandos:

```
pip install gensim 
easy_install gensim
```

Vamos a utilizar un conjunto de datos de noticias de Associated Press (AP). Este es un conjunto de datos estándar, que se utilizó en algunos de los trabajos iniciales sobre modelos de temas:

```python
>>> from gensim import corpora, models, similarities
>>> corpus = corpora.BleiCorpus('./data/ap/ap.dat', '/data/ap/vocab.txt')
```

Vamos a utilizar un conjunto de datos de noticias de Associated Press (AP). Este es un conjunto de datos estándar, que se utilizó en algunos de los trabajos iniciales sobre modelos de temas:


```python
from __future__ import print_function

try:
    from gensim import corpora, models, matutils
except:
    print("import gensim failed.")
    print()
    print("Please install it")
    raise

import matplotlib.pyplot as plt
import numpy as np
from os import path

NUM_TOPICS = 100


if not path.exists('./data/ap/ap.dat'):
    print('Error: Expected data to be present at data/ap/')
    print('Please cd into ./data & run ./download_ap.sh')


corpus = corpora.BleiCorpus('./data/ap/ap.dat', './data/ap/vocab.txt')


model = models.ldamodel.LdaModel(
    corpus, num_topics=NUM_TOPICS, id2word=corpus.id2word, alpha=None)


for ti in range(model.num_topics):
    words = model.show_topic(ti, 64)
    tf = sum(f for _, f in words)
    with open('topics.txt', 'w') as output:
        output.write('\n'.join('{}:{}'.format(w, int(1000. * f / tf)) for w, f in words))
        output.write("\n\n\n")


topics = matutils.corpus2dense(model[corpus], num_terms=model.num_topics)
weight = topics.sum(1)
max_topic = weight.argmax()


words = model.show_topic(max_topic, 64)

from __future__ import print_function
warned_of_error = False

def create_cloud(oname, words,maxsize=120, fontname='Lobster'):
    '''Creates a word cloud (when pytagcloud is installed)
    Parameters
    ----------
    oname : output filename
    words : list of (value,str)
    maxsize : int, optional
        Size of maximum word. The best setting for this parameter will often
        require some manual tuning for each input.
    fontname : str, optional
        Font to use.
    '''
    try:
        from pytagcloud import create_tag_image, make_tags
    except ImportError:
        if not warned_of_error:
            print("No se pudo importar pytagcloud. Saltando la generación de nube")
        return

    words = [(w,int(v*10000)) for w,v in words]
    tags = make_tags(words, maxsize=maxsize)
    create_tag_image(tags, oname, size=(1800, 1200), fontname=fontname)

create_cloud('../img/image1.png', words)

num_topics_used = [len(model[doc]) for doc in corpus]
fig,ax = plt.subplots()
ax.hist(num_topics_used, np.arange(42))
ax.set_ylabel('Nr of documents')
ax.set_xlabel('Nr of topics')
fig.tight_layout()
fig.savefig('../img/image2.png')



ALPHA = 1.0

model1 = models.ldamodel.LdaModel(
    corpus, num_topics=NUM_TOPICS, id2word=corpus.id2word, alpha=ALPHA)
num_topics_used1 = [len(model1[doc]) for doc in corpus]

fig,ax = plt.subplots()
ax.hist([num_topics_used, num_topics_used1], np.arange(42))
ax.set_ylabel('Nr of documents')
ax.set_xlabel('Nr of topics')

ax.text(9, 223, r'default alpha')
ax.text(26, 156, 'alpha=1.0')
fig.tight_layout()
fig.savefig('../img/image3.png')
```

Podemos ver que solo se utilizan unos pocos temas para cada documento. El modelo de tema es un modelo disperso, ya que aunque hay muchos temas posibles para cada documento, solo se usan algunos de ellos. Podemos trazar un histograma del número de temas como se muestra en el siguiente gráfico:

![](img/image2.png)


En este caso, este es un alfa más grande, que debería llevar a más temas por documento. También podríamos usar un valor menor. Como podemos ver en el histograma combinado que se muestra a continuación, gensim se comporta como esperábamos:

![](img/image3.png)




## Comparando la similitud en el espacio temático

Usando gensim, vimos antes cómo calcular los temas correspondientes a todos los documentos en el corpus:


```python
>>> topics = [model[c] for c in corpus]
>>> print topics[0]
[(3, 0.023607255776894751),
 (13, 0.11679936618551275),
 (19, 0.075935855202707139),
 (92, 0.10781541687001292)]
```

Almacenaremos todos estos recuentos de temas en matrices NumPy y calcularemos todas las distancias en pares:


```python
>>> dense = np.zeros( (len(topics), 100), float)
>>> for ti,t in enumerate(topics):
…   for tj,v in t:
…   dense[ti,tj] = v
```

Ahora, dense es una matriz de temas. Podemos usar la función pdist en SciPy para calcular todas las distancias en pares. Es decir, con una sola llamada de función, calculamos todos los valores de suma ((denso [ti] - denso [tj]) ** 2):


```python
>>> from scipy.spatial import distance
>>> pairwise = distance.squareform(distance.pdist(dense))
```

Ahora empleamos un último truco pequeño; establecemos los elementos diagonales de la matriz de distancia en un valor alto (solo necesita ser más grande que los otros valores de la matriz):

```python
>>> largest = pairwise.max()
>>> for ti in range(len(topics)): 
    pairwise[ti,ti] = largest+1
```

Para cada documento, podemos buscar fácilmente el elemento más cercano:

```python
>>> def closest_to(doc_id):
return pairwise[doc_id].argmin()
```




### Modelando la totalidad de Wikipedia

```python
from __future__ import print_function
import numpy as np
import gensim
from os import path

from __future__ import print_function
warned_of_error = False

def create_cloud(oname, words,maxsize=120, fontname='Lobster'):
    '''Creates a word cloud (when pytagcloud is installed)
    Parameters
    ----------
    oname : output filename
    words : list of (value,str)
    maxsize : int, optional
        Size of maximum word. The best setting for this parameter will often
        require some manual tuning for each input.
    fontname : str, optional
        Font to use.
    '''
    try:
        from pytagcloud import create_tag_image, make_tags
    except ImportError:
        if not warned_of_error:
            print("No se pudo importar pytagcloud. Saltando la generación de nube")
        return


    words = [(w,int(v*10000)) for w,v in words]
    tags = make_tags(words, maxsize=maxsize)
    create_tag_image(tags, oname, size=(1800, 1200), fontname=fontname)

if not path.exists('wiki_lda.pkl'):
    import sys
    sys.stderr.write('''\
This script must be run after wikitopics_create.py!
That script creates and saves the LDA model (this must onlly be done once).
This script is responsible for the analysis.''')
    sys.exit(1)


id2word = gensim.corpora.Dictionary.load_from_text(
    'data/wiki_en_output_wordids.txt.bz2')
mm = gensim.corpora.MmCorpus('data/wiki_en_output_tfidf.mm')


model = gensim.models.ldamodel.LdaModel.load('wiki_lda.pkl')

topics = np.load('topics.npy', mmap_mode='r')


lens = (topics > 0).sum(axis=1)
print('Mean number of topics mentioned: {0:.3}'.format(np.mean(lens)))
print('Percentage of articles mentioning less than 10 topics: {0:.1%}'.format(np.mean(lens <= 10)))


weights = topics.sum(0)


words = model.show_topic(weights.argmax(), 64)


create_cloud('../img/image4.png', words, maxsize=250, fontname='Cardo')

fraction_mention = np.mean(topics[:,weights.argmax()] > 0)
print("The most mentioned topics is mentioned in {:.1%} of documents.".format(fraction_mention))
total_weight = np.mean(topics[:,weights.argmax()])
print("It represents {:.1%} of the total number of words.".format(total_weight))
print()
print()
print()


words = model.show_topic(weights.argmin(), 64)
create_cloud('../img/image5.png', words, maxsize=150, fontname='Cardo')
fraction_mention = np.mean(topics[:,weights.argmin()] > 0)
print("The least mentioned topics is mentioned in {:.1%} of documents.".format(fraction_mention))
total_weight = np.mean(topics[:,weights.argmin()])
print("It represents {:.1%} of the total number of words.".format(total_weight))
print()
print()
print()
```

![](img/image4.png)



![](img/image5.png)
