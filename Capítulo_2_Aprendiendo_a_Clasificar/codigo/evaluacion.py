import numpy as np
def fit_model(features, labels):
    '''Learn a simple threshold model'''
    best_acc = -1.0

    for fi in range(features.shape[1]):
        thresh = features[:, fi].copy()

        thresh.sort()
        for t in thresh:
            pred = (features[:, fi] > t)


            acc = (pred == labels).mean()

            rev_acc = (pred == ~labels).mean()
            if rev_acc > acc:
                acc = rev_acc
                reverse = True
            else:
                reverse = False
            if acc > best_acc:
                best_acc = acc
                best_fi = fi
                best_t = t
                best_reverse = reverse


    return best_t, best_fi, best_reverse



def predict(model, features):
    '''Apply a learned model'''

    t, fi, reverse = model
    if reverse:
        return features[:, fi] <= t
    else:
        return features[:, fi] > t

def accuracy(features, labels, model):
    '''Compute the accuracy of the model'''
    preds = predict(model, features)
    return np.mean(preds == labels)




import numpy as np
from sklearn.datasets import load_iris


data = load_iris()
features = data['data']
labels = data['target_names'][data['target']]


is_setosa = (labels == 'setosa')
features = features[~is_setosa]
labels = labels[~is_setosa]


is_virginica = (labels == 'virginica')


testing = np.tile([True, False], 50) 


training = ~testing

model = fit_model(features[training], is_virginica[training])
train_accuracy = accuracy(features[training], is_virginica[training], model)
test_accuracy = accuracy(features[testing], is_virginica[testing], model)

print('''\
Training accuracy was {0:.1%}.
Testing accuracy was {1:.1%} (N = {2}).
'''.format(train_accuracy, test_accuracy, testing.sum()))


####

correct = 0.0
for ei in range(len(features)):
    training = np.ones(len(features), bool)
    training[ei] = False
    testing = ~training
    model = fit_model(features[training], is_virginica[training])
    predictions = predict(model, features[testing])
    correct += np.sum(predictions == is_virginica[testing])



acc = correct/float(len(features))
print('Accuracy: {0:.1%}'.format(acc))


