# Capítulo 2: Aprendiendo a clasificar con ejemplos del mundo real

## El dataset de Iris

El conjunto de datos Iris es un conjunto de datos clásico de la década de 1930; Es uno de los primeros ejemplos modernos de clasificación estadística.

El escenario es el de las flores de Iris, de las cuales existen múltiples especies que pueden identificarse por su morfología. 

Se midieron los siguientes cuatro atributos de cada planta:

* Sepal length
* Sepal width
* Petal length
* Petal width

En general, llamaremos cualquier medida de nuestros datos como características.
La pregunta ahora es: si viéramos una nueva flor en el campo, ¿podríamos hacer una buena predicción sobre sus especies a partir de sus medidas?
Este es el problema de aprendizaje supervisado o clasificación; dados los ejemplos etiquetados, podemos diseñar una regla que eventualmente se aplicará a otros ejemplos.
El conjunto de datos de Iris cumple con nuestros propósitos. Es pequeño (150 ejemplos, 4 características cada uno) y se puede visualizar y manipular fácilmente.

### El primer paso es la visualización.

El grupo periférico (triángulos) son las plantas Iris Setosa, mientras que las plantas Iris Versicolor están en el centro (círculo) e Iris Virginica se indica con marcas "x".

![](img/image1.png)


```python
from matplotlib import pyplot as plt
from sklearn.datasets import load_iris
import numpy as np

data = load_iris()
features = data.data
feature_names = data.feature_names
target = data.target
target_names = data.target_names

fig,axes = plt.subplots(2, 3)
pairs = [(0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)]

color_markers = [
        ('r', '>'),
        ('g', 'o'),
        ('b', 'x'),
        ]

for i, (p0, p1) in enumerate(pairs):
    ax = axes.flat[i]

    for t in range(3):
        c,marker = color_markers[t]
        ax.scatter(features[target == t, p0], features[
                    target == t, p1], marker=marker, c=c)
    ax.set_xlabel(feature_names[p0])
    ax.set_ylabel(feature_names[p1])
    ax.set_xticks([])
    ax.set_yticks([])

fig.tight_layout()
fig.savefig('../img/image1.png')
```

### Construyendo nuestro primer modelo de clasificación

Si el objetivo es separar los tres tipos de flores, podemos hacer algunas sugerencias de inmediato. Por ejemplo, la longitud del pétalo parece poder separar a Iris Setosa de las otras dos especies de flores por sí sola. Podemos escribir un poco de código para descubrir dónde está el corte:


```python
labels = target_names[target]
plength = features[:, 2]
is_setosa = (labels == 'setosa')
max_setosa =plength[is_setosa].max() 
min_non_setosa = plength[~is_setosa].min()
print('Maximum of setosa: {0}.'.format(max_setosa)) 
print('Minimum of others: {0}.'.format(min_non_setosa))
```

 el mejor modelo que obtenemos se divide en la longitud del pétalo. Podemos visualizar el límite de decisión. En la siguiente captura de pantalla, vemos dos regiones: una es blanca y la otra está sombreada en gris. Cualquier cosa que caiga en la región blanca se llamará Iris Virginica y cualquier cosa que caiga en el lado sombreado se clasificará como Iris Versicolor:


![](img/image2.png)

```python
COLOUR_FIGURE = False
from matplotlib import pyplot as plt
from sklearn.datasets import load_iris
data = load_iris()
features = data.data
feature_names = data.feature_names
target = data.target
target_names = data.target_names


labels = target_names[target]

is_setosa = (labels == 'setosa')
features = features[~is_setosa]
labels = labels[~is_setosa]
is_virginica = (labels == 'virginica')


t = 1.65
t2 = 1.75


f0, f1 = 3, 2

if COLOUR_FIGURE:
    area1c = (1., .8, .8)
    area2c = (.8, .8, 1.)
else:
    area1c = (1., 1, 1)
    area2c = (.7, .7, .7)


x0 = features[:, f0].min() * .9
x1 = features[:, f0].max() * 1.1

y0 = features[:, f1].min() * .9
y1 = features[:, f1].max() * 1.1

fig,ax = plt.subplots()
ax.fill_between([t, x1], [y0, y0], [y1, y1], color=area2c)
ax.fill_between([x0, t], [y0, y0], [y1, y1], color=area1c)
ax.plot([t, t], [y0, y1], 'k--', lw=2)
ax.plot([t2, t2], [y0, y1], 'k:', lw=2)
ax.scatter(features[is_virginica, f0],
            features[is_virginica, f1], c='b', marker='o', s=40)
ax.scatter(features[~is_virginica, f0],
            features[~is_virginica, f1], c='r', marker='x', s=40)
ax.set_ylim(y0, y1)
ax.set_xlim(x0, x1)
ax.set_xlabel(feature_names[f0])
ax.set_ylabel(feature_names[f1])
fig.tight_layout()
fig.savefig('../img/image2.png')
```



```python
best_acc = -1.0
for fi in range(features.shape[1]):
    thresh = features[:,fi]
    for t in thresh:


        feature_i = features[:, fi]
        # apply threshold `t`
        pred = (feature_i > t)
        acc = (pred == is_virginica).mean()
        rev_acc = (pred == ~is_virginica).mean()
        if rev_acc > acc:
            reverse = True
            acc = rev_acc
        else:
            reverse = False

        if acc > best_acc:
            best_acc = acc
            best_fi = fi
            best_t = t
            best_reverse = reverse
print(best_fi, best_t, best_reverse, best_acc)
```



#### Evaluación - sosteniendo datos y validación cruzada

El modelo discutido en la sección anterior es un modelo simple; alcanza el 94 por ciento de precisión en sus datos de entrenamiento. Sin embargo, esta evaluación puede ser demasiado optimista. Utilizamos los datos para definir cuál sería el umbral y luego utilizamos los mismos datos para evaluar el modelo. Por supuesto, el modelo funcionará mejor que cualquier otra cosa que hayamos probado en este conjunto de datos. La lógica es circular.



```python
import numpy as np
def fit_model(features, labels):
    '''Learn a simple threshold model'''
    best_acc = -1.0

    for fi in range(features.shape[1]):
        thresh = features[:, fi].copy()

        thresh.sort()
        for t in thresh:
            pred = (features[:, fi] > t)


            acc = (pred == labels).mean()

            rev_acc = (pred == ~labels).mean()
            if rev_acc > acc:
                acc = rev_acc
                reverse = True
            else:
                reverse = False
            if acc > best_acc:
                best_acc = acc
                best_fi = fi
                best_t = t
                best_reverse = reverse


    return best_t, best_fi, best_reverse


def predict(model, features):
    '''Apply a learned model'''

    t, fi, reverse = model
    if reverse:
        return features[:, fi] <= t
    else:
        return features[:, fi] > t

def accuracy(features, labels, model):
    '''Compute the accuracy of the model'''
    preds = predict(model, features)
    return np.mean(preds == labels)


import numpy as np
from sklearn.datasets import load_iris


data = load_iris()
features = data['data']
labels = data['target_names'][data['target']]


is_setosa = (labels == 'setosa')
features = features[~is_setosa]
labels = labels[~is_setosa]


is_virginica = (labels == 'virginica')


testing = np.tile([True, False], 50) 


training = ~testing

model = fit_model(features[training], is_virginica[training])
train_accuracy = accuracy(features[training], is_virginica[training], model)
test_accuracy = accuracy(features[testing], is_virginica[testing], model)

print('''\
Training accuracy was {0:.1%}.
Testing accuracy was {1:.1%} (N = {2}).
'''.format(train_accuracy, test_accuracy, testing.sum()))
```

Lo que realmente queremos hacer es estimar la capacidad del modelo para generalizar a nuevas instancias. Deberíamos medir su desempeño en casos que el algoritmo no haya visto en el entrenamiento. Por lo tanto, vamos a hacer una evaluación más rigurosa y utilizaremos los datos retenidos. Para esto, vamos a dividir los datos en dos bloques: en un bloque, entrenaremos el modelo y en el otro, el que no entrenamos, lo probaremos. La salida es la siguiente:


```
Training accuracy was 96.0%.
Testing accuracy was 90.0% (N = 50).
```


Podemos lograr algo bastante similar mediante la validación cruzada. Una forma extrema (pero a veces útil) de validación cruzada es dejar-uno-fuera. Tomaremos un ejemplo de los datos de entrenamiento, aprenderemos un modelo sin este ejemplo y luego veremos si el modelo clasifica este ejemplo correctamente:


```python
correct = 0.0
for ei in range(len(features)):
    training = np.ones(len(features), bool)
    training[ei] = False
    testing = ~training
    model = fit_model(features[training], is_virginica[training])
    predictions = predict(model, features[testing])
    correct += np.sum(predictions == is_virginica[testing])

acc = correct/float(len(features))
print('Accuracy: {0:.1%}'.format(acc))
```


```
Accuracy: 87.0%
```



## Construyendo clasificadores más complejos

¿Qué conforma un modelo de clasificación? Podemos dividirlo en tres partes:
* **La estructura del modelo:** en esto, usamos un umbral en una sola característica.
* **El procedimiento de búsqueda:** En esto, probamos todas las combinaciones posibles de característica y umbral.
* **La función de pérdida:** Al usar la función de pérdida, decidimos cuál de las posibilidades es menos mala (porque rara vez podemos hablar de la solución perfecta). Podemos usar el error de entrenamiento o simplemente definir este punto al revés y decir que queremos la mejor precisión. Tradicionalmente, la gente quiere que la función de pérdida
ser minimo


## Un dataset más complejo y un clasificador más complejo


### Clasificación de vecionos más cercanos

Con este conjunto de datos, incluso si intentamos separar dos clases utilizando el método anterior, no obtenemos muy buenos resultados. Permítanme presentarles, por lo tanto, un nuevo clasificador: el clasificador vecino más cercano.



En este caso, nuestro modelo implica guardar todos los datos y etiquetas de entrenamiento y computar todo en el momento de la clasificación. Una mejor implementación sería en realidad indexar estos en el tiempo de aprendizaje para acelerar la clasificación, pero esta implementación es un algoritmo complejo.

Ahora, tenga en cuenta que este modelo funciona perfectamente en sus datos de entrenamiento! Para cada punto, su vecino más cercano es él mismo, por lo que su etiqueta coincide perfectamente (a menos que dos ejemplos tengan exactamente las mismas características pero diferentes etiquetas, lo que puede suceder).


```python
def load_dataset(dataset_name):
    '''
    data,labels = load_dataset(dataset_name)
    Load a given dataset
    Returns
    -------
    data : numpy ndarray
    labels : list of str
    '''
    data = []
    labels = []
    with open('../data/{0}.tsv'.format(dataset_name)) as ifile:
        for line in ifile:
            tokens = line.strip().split('\t')
            data.append([float(tk) for tk in tokens[:-1]])
            labels.append(tokens[-1])
    data = np.array(data)
    labels = np.array(labels)
    return data, labels


import numpy as np


def fit_model(k, features, labels):
    '''Learn a k-nn model'''
    # There is no model in k-nn, just a copy of the inputs
    return k, features.copy(), labels.copy()


def plurality(xs):
    '''Find the most common element in a collection'''
    from collections import defaultdict
    counts = defaultdict(int)
    for x in xs:
        counts[x] += 1
    maxv = max(counts.values())
    for k, v in counts.items():
        if v == maxv:
            return k


def predict(model, features):
    '''Apply k-nn model'''
    k, train_feats, labels = model
    results = []
    for f in features:
        label_dist = []
        # Compute all distances:
        for t, ell in zip(train_feats, labels):
            label_dist.append((np.linalg.norm(f - t), ell))
        label_dist.sort(key=lambda d_ell: d_ell[0])
        label_dist = label_dist[:k]
        results.append(plurality([ell for _, ell in label_dist]))
    return np.array(results)


def accuracy(features, labels, model):
    preds = predict(model, features)
    return np.mean(preds == labels)


COLOUR_FIGURE = False

from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
import numpy as np


feature_names = [
    'area',
    'perimeter',
    'compactness',
    'length of kernel',
    'width of kernel',
    'asymmetry coefficien',
    'length of kernel groove',
]


def plot_decision(features, labels):
    '''Plots decision boundary for KNN
    Parameters
    ----------
    features : ndarray
    labels : sequence
    Returns
    -------
    fig : Matplotlib Figure
    ax  : Matplotlib Axes
    '''
    y0, y1 = features[:, 2].min() * .9, features[:, 2].max() * 1.1
    x0, x1 = features[:, 0].min() * .9, features[:, 0].max() * 1.1
    X = np.linspace(x0, x1, 100)
    Y = np.linspace(y0, y1, 100)
    X, Y = np.meshgrid(X, Y)

    model = fit_model(1, features[:, (0, 2)], np.array(labels))
    C = predict(
        model, np.vstack([X.ravel(), Y.ravel()]).T).reshape(X.shape)
    if COLOUR_FIGURE:
        cmap = ListedColormap([(1., .6, .6), (.6, 1., .6), (.6, .6, 1.)])
    else:
        cmap = ListedColormap([(1., 1., 1.), (.2, .2, .2), (.6, .6, .6)])
    fig,ax = plt.subplots()
    ax.set_xlim(x0, x1)
    ax.set_ylim(y0, y1)
    ax.set_xlabel(feature_names[0])
    ax.set_ylabel(feature_names[2])
    ax.pcolormesh(X, Y, C, cmap=cmap)
    if COLOUR_FIGURE:
        cmap = ListedColormap([(1., .0, .0), (.0, 1., .0), (.0, .0, 1.)])
        ax.scatter(features[:, 0], features[:, 2], c=labels, cmap=cmap)
    else:
        for lab, ma in zip(range(3), "Do^"):
            ax.plot(features[labels == lab, 0], features[
                     labels == lab, 2], ma, c=(1., 1., 1.))
    return fig,ax


features, labels = load_dataset('seeds')
names = sorted(set(labels))
labels = np.array([names.index(ell) for ell in labels])

fig,ax = plot_decision(features, labels)
fig.savefig('../img/image4.png')

features -= features.mean(0)
features /= features.std(0)
fig,ax = plot_decision(features, labels)
fig.savefig('../img/image5.png')
```

![](img/image4.png)

Los ejemplos canadienses se muestran como diamantes, las semillas de Kama como círculos y las semillas de Rosa como triángulos. Sus áreas respectivas se muestran como blanco, negro y gris. Quizás te preguntes por qué las regiones son tan horizontales, casi de manera extraña. El problema es que el eje x (área) varía de 10 a 22, mientras que el eje y (compacidad) varía de 0,75 a 1,0. Esto significa que un pequeño cambio en x es en realidad mucho más grande que un pequeño cambio en y. Entonces, cuando calculamos la distancia de acuerdo con la función anterior, en su mayor parte, solo estamos tomando en cuenta el eje x.


![](img/image5.png)

Ahora, cada característica está en la misma unidad (técnicamente, cada característica ahora no tiene dimensiones; no tiene unidades) y podemos mezclar las dimensiones con más confianza. De hecho, si ahora ejecutamos el clasificador de nuestro vecino más cercano, obtenemos el 94% de precisión.

Los límites ahora son mucho más complejos y hay interacción entre las dos dimensiones. En el conjunto completo de datos, todo está sucediendo en un espacio de siete dimensiones que es muy difícil de visualizar, pero se aplica el mismo principio: donde antes de unas pocas dimensiones eran dominantes, ahora a todos se les da la misma importancia.


