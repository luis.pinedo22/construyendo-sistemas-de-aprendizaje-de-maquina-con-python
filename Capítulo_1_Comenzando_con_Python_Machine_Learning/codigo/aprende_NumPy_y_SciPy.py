import numpy
numpy.version.full_version 

import numpy as np
a = np.array([0,1,2,3,4,5])
a
a.ndim
a.shape

b = a.reshape((3,2))
b
b.ndim 
b.shape

b[1][0]=77
b
a

c = a.reshape((3,2)).copy()
c
c[0][0] = -99
a
c

a*2
a**2
[1,2,3,4,5]*2

a[np.array([2,3,4])]

a>4
a[a>4]

a[a>4] = 4
a

a.clip(0,4)

c = np.array([1, 2, np.NAN, 3, 4])
c
np.isnan(c)
c[~np.isnan(c)]
np.mean(c[~np.isnan(c)])

import timeit
normal_py_sec = timeit.timeit('sum(x*x for x in range(1000))', number=10000) 
naive_np_sec = timeit.timeit('sum(na*na)', setup="import numpy as np; na=np.arange(1000)", number=10000)
good_np_sec = timeit.timeit('na.dot(na)', setup="import numpy as np; na=np.arange(1000)", number=10000)
print("Normal Python: %f sec"%normal_py_sec) 
print("Naive NumPy: %f sec"%naive_np_sec) 
print("Good NumPy: %f sec"%good_np_sec)

a = np.array([1,2,3])
a.dtype

np.array([1, "stringy"])
np.array([1, "stringy", set([1,2,3])])

