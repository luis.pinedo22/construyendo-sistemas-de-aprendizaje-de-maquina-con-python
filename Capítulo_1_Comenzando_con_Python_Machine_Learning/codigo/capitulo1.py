import os
import scipy as sp
import matplotlib.pyplot as plt

sp.random.seed(3)

data = sp.genfromtxt("data/web_traffic.tsv", delimiter="\t")

print(data[:10])

print(data.shape)

colors = ['g', 'k', 'b', 'm', 'r']
linestyles = ['-', '-.', '--', ':', '-']

x = data[:, 0]
y = data[:, 1]

print("Número de entradas inválidas:", sp.sum(sp.isnan(y)))
x = x[~sp.isnan(y)]
y = y[~sp.isnan(y)]

def plot_models(x, y, models, fname, mx=None, ymax=None, xmin=None):
    ''' plot input data '''

    plt.figure(num=None, figsize=(8, 6))
    plt.clf()
    plt.scatter(x, y, s=10)
    plt.title("Tráfico web durante el último mes")
    plt.xlabel("Tiempo")
    plt.ylabel("éxito/hora")
    plt.xticks(
        [w * 7 * 24 for w in range(10)], ['semana %i' % w for w in range(10)])

    if models:
        if mx is None:
            mx = sp.linspace(0, x[-1], 1000)
        for model, style, color in zip(models, linestyles, colors):
            # print "Model:",model
            # print "Coeffs:",model.coeffs
            plt.plot(mx, model(mx), linestyle=style, linewidth=3, c=color)

        plt.legend(["d=%i" % m.order for m in models], loc="upper left")

    plt.autoscale(tight=True)
    plt.ylim(ymin=0)
    if ymax:
        plt.ylim(ymax=ymax)
    if xmin:
        plt.xlim(xmin=xmin)
    plt.grid(True, linestyle='-', color='0.75')
    plt.savefig(fname)


plot_models(x, y, None, os.path.join("1400_01_01.png"))

fp1, res1, rank1, sv1, rcond1 = sp.polyfit(x, y, 1, full=True)

print("Parámetros del modelo de fp1: %s" % fp1)
print("Error del modelo de fp1:", res1)
f1 = sp.poly1d(fp1)

fp2, res2, rank2, sv2, rcond2 = sp.polyfit(x, y, 2, full=True)
print("Parámetros del modelo de fp2: %s" % fp2)
print("Error del modelo de fp2:", res2)

f2 = sp.poly1d(fp2)
f3 = sp.poly1d(sp.polyfit(x, y, 3))
f10 = sp.poly1d(sp.polyfit(x, y, 10))
f100 = sp.poly1d(sp.polyfit(x, y, 100))

plot_models(x, y, [f1], os.path.join("1400_01_02.png"))

plot_models(x, y, [f1, f2], os.path.join("1400_01_03.png"))

plot_models(
    x, y, [f1, f2, f3, f10, f100], os.path.join("1400_01_04.png"))


inflection = 3.5 * 7 * 24
xa = x[:int(inflection)]
ya = y[:int(inflection)]
xb = x[int(inflection):]
yb = y[int(inflection):]
xa = list(map(int, xa))
ya = list(map(int, ya))
xb = list(map(int, xb))
yb = list(map(int, yb))


fa = sp.poly1d(sp.polyfit(xa, ya, 1))
fb = sp.poly1d(sp.polyfit(xb, yb, 1))

plot_models(x, y, [fa, fb], os.path.join("1400_01_05.png"))

def error(f, x, y):
    return sp.sum((f(x) - y) ** 2)


print("Errores para el conjunto completo de datos:")
for f in [f1, f2, f3, f10, f100]:
    print("Error d=%i: %f" % (f.order, error(f, x, y)))


print("Errores solo por el tiempo después del punto de inflexión")
for f in [f1, f2, f3, f10, f100]:
    print("Error d=%i: %f" % (f.order, error(f, xb, yb)))

print("Inflexión de error=%f" % (error(fa, xa, ya) + error(fb, xb, yb)))

plot_models(
    x, y, [f1, f2, f3, f10, f100],
    os.path.join("1400_01_06.png"),
    mx=sp.linspace(0 * 7 * 24, 6 * 7 * 24, 100),
    ymax=10000, xmin=0 * 7 * 24)

print("Entrenado solo en datos después del punto de inflexión")
fb1 = fb
fb2 = sp.poly1d(sp.polyfit(xb, yb, 2))
fb3 = sp.poly1d(sp.polyfit(xb, yb, 3))
fb10 = sp.poly1d(sp.polyfit(xb, yb, 10))
fb100 = sp.poly1d(sp.polyfit(xb, yb, 100))

print("Errores solo por el tiempo después del punto de inflexión")
for f in [fb1, fb2, fb3, fb10, fb100]:
    print("Error d=%i: %f" % (f.order, error(f, xb, yb)))


plot_models(
    x, y, [fb1, fb2, fb3, fb10, fb100],
    os.path.join("1400_01_07.png"),
    mx=sp.linspace(0 * 7 * 24, 6 * 7 * 24, 100),
    ymax=10000, xmin=0 * 7 * 24)

frac = 0.3
split_idx = int(frac * len(xb))
shuffled = sp.random.permutation(list(range(0, len(xb))))
test = sorted(shuffled[:split_idx])
train = sorted(shuffled[split_idx:])

xb = set([xb[x] for x in range(len(train))])
xb = list(xb)

yb = set([yb[x] for x in range(len(train))])
yb = list(yb)


fbt1 = sp.poly1d(sp.polyfit(xb, yb, 1))
fbt2 = sp.poly1d(sp.polyfit(xb, yb, 2))

print("fbt2(x)= \n%s" % fbt2)
print("fbt2(x)-100,000= \n%s" % (fbt2-100000))


fbt3 = sp.poly1d(sp.polyfit(xb, yb, 3))
fbt10 = sp.poly1d(sp.polyfit(xb, yb, 10))
fbt100 = sp.poly1d(sp.polyfit(xb, yb, 100))


print("Errores de prueba solo por el tiempo después del punto de inflexión")
for f in [fbt1, fbt2, fbt3, fbt10, fbt100]:
    print("Error d=%i: %f" % (f.order, error(f, xb, yb)))

plot_models(
    x, y, [fbt1, fbt2, fbt3, fbt10, fbt100],
    os.path.join("1400_01_08.png"),
    mx=sp.linspace(0 * 7 * 24, 6 * 7 * 24, 100),
    ymax=10000, xmin=0 * 7 * 24)

from scipy.optimize import fsolve
print(fbt2)
print(fbt2 - 100000)
reached_max = fsolve(fbt2 - 100000, x0=800) / (7 * 24)
print("100,000 hits/hour expected at week %f" % reached_max[0])


