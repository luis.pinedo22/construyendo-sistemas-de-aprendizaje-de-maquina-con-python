# Capítulo 1: Comenzando con Python Machine Learning

## ¿Qué es Machine Learning?

El Machine Learning o aprendizaje de máquina enseña a las máquinas cómo realizar tareas por sí mismas. Una máquina aprende de manera de que se le tiene que proporcionar una par de ejemplos de como realizar la tarea en especifico. El aprendizaje automático también es conocido como minería de datos o análisis predictivo  Existe una gran cantidad de algoritmos de aprendizaje de máquina que nos ayudan a resolver problemas complejos relacionados con grandes cantidades de datos.

El aprendizaje automático se presta perfectamente a Python ya que, este es un lenguaje de programación de alto nivel interpretado, puede parecer que Python fue diseñado específicamente para el proceso de probar cosas diferentes.

Este libro proporciona los algoritmos de aprendizaje que se utilizan actualmente en los diversos campos del aprendizaje automático y qué debe tener en cuenta al aplicarlos. Las actividades que se realizaran son las siguientes.

1. Leer los datos y limpiarlos.
2. Exploración y comprensión de los datos de entrada.
3. Analizar cómo presentar mejor los datos al algoritmo de aprendizaje.
4. Elegir el modelo correcto y el algoritmo de aprendizaje.
5. Medir el rendimiento correctamente.

### Bibliotecas básicas para Python

Primero lo que tenemos que tener instalado en nuestra máquina es Python. Después tenemos que instalar NumPy y SciPy para operaciones numéricas, así como Matplotlib para visualización.

Puedes consultar la documentación de estas bibliotecas:
* [NumPy](http://www.numpy.org)
* [SciPy](http://scipy.org)
* [Matplotlib](http://matplotlib.org)

### Aprendiendo NumPy


Se importa NumPy de la siguiente manera y podemos ver la versión que tenemos.

```python
>>> import numpy
>>> numpy.version.fullversion 
```

Se importa NumPy para hacer uso de la matriz numpy.array

```python
>>> import numpy as np
>> a = np.array([0,1,2,3,4,5])
>>> aarray([0, 1, 2, 3, 4, 5])
>>> a.ndim
1
>>> a.shape
(6,)
```

Acabamos de crear una matriz de manera similar a como crearíamos una lista en Python.

Ahora podemos transformar esta matriz en una matriz 2D de la siguiente manera:

```python
>>> b = a.reshape((3,2))
>>> b
array([[0, 1],
       [2, 3],
       [4, 5]])
>>> b.ndim 2
>>> b.shape
(3, 2)
```

El paquete NumPy está optimizado para evita copias siempre que sea posible.

Como por ejemplo se remplazo el número '2' por el '77' en la fila fila 1 y en la columna 0.

```python
b[1][0]=77
b
array([[ 0, 1],
       [77, 3],
       [ 4, 5]])
​
a
array([ 0, 1, 77, 3, 4, 5])
```

Se pueden hacer copia totalmente independientes, en donde el array 'a' no se ve afectado por el cambio que se hizo en el array 'c'.


```python
>>> c = a.reshape((3,2)).copy()
>>> c
array([[ 0, 1],
       [77, 3],
       [ 4, 5]])
>>> c[0][0] = -99
>>> a
array([ 0, 1, 77, 3, 4, 5])
>>> c
array([[-99, 1],
       [77, 3],
       [4, 5]])
```

Otra gran ventaja de las matrices NumPy es que las operaciones se propagan a los elementos individuales.


```python
>>> a*2
array([ 2, 4, 6, 8, 10])
>>> a**2
array([ 1, 4, 9, 16, 25])
>>> [1,2,3,4,5]*2
[1, 2, 3, 4, 5, 1, 2, 3, 4, 5]
```


### Indexación

NumPy no ofrece formas versátiles en las que se puede acceder a sus arreglos.


```python
>>> a[np.array([2,3,4])]
array([77, 3, 4])
```

Se pueden colocar condiciones, en donde aplica a los elementos individuales, obtenemos una forma muy conveniente de acceder a nuestros datos.

```python
>>> a[a>4] = 4
>>> a
array([0, 1, 4, 3, 4, 4])
```

Como este es un caso de uso frecuente, hay una función de clip especial para él, recortando los valores en ambos extremos de un intervalo con una llamada de función de la siguiente manera:


```python
>>> a.clip(0,4)
array([0, 1, 4, 3, 4, 4])
```


### Manejo de valores no existentes

Las capacidades de indexación de NumPy es útil cuando se procesan previamente los datos que acabamos de leer de un archivo de texto. Lo más probable es que contenga valores no válidos, que marcaremos como no siendo un número real usando numpy.NAN de la siguiente manera:

```python
c = np.array([1, 2, np.NAN, 3, 4])
>>> c
array([ 1., 2., nan, 3., 4.])
>>> np.isnan(c)
array([False, False, True, False, False], dtype=bool)
>>> c[~np.isnan(c)]
array([ 1., 2., 3., 4.])
>>> np.mean(c[~np.isnan(c)])
2.5
```

### Comparando comportamientos de tiempo de ejecución

Comparemos el comportamiento en tiempo de ejecución de NumPy con las listas normales de Python. En el siguiente código, calcularemos la suma de todos los números cuadrados de 1 a 1000 y veremos cuánto tiempo tomará el cálculo. Lo hacemos 10000 veces e informamos el tiempo total para que nuestra medición sea lo suficientemente precisa.

```python
import timeit
normalpysec = timeit.timeit('sum(x*x for x in range(1000))', number=10000) 
naivenpsec = timeit.timeit('sum(na*na)', setup="import numpy as np; na=np.arange(1000)", number=10000)
goodnpsec = timeit.timeit('na.dot(na)', setup="import numpy as np; na=np.arange(1000)", number=10000)
​
print("Normal Python: %f sec"%normalpysec) 
print("Naive NumPy: %f sec"%naivenpsec) 
print("Good NumPy: %f sec"%goodnpsec)
​
Normal Python: 1.157467 sec
Naive NumPy: 4.061293 sec
Good NumPy: 0.033419 sec
```

Hacemos dos observaciones interesantes. Primero, solo usar NumPy como almacenamiento de datos (Naive NumPy) toma 3.5 veces más, lo cual es sorprendente, ya que creemos que debe ser mucho más rápido, ya que está escrito como una extensión C. Una razón para esto es que el acceso de elementos individuales desde Python en sí es bastante costoso. Solo cuando somos capaces de aplicar algoritmos dentro del código de extensión optimizado, obtenemos mejoras de velocidad y tremendas: usando la función punto () de NumPy, somos más de 25 veces más rápidos. En resumen, en cada algoritmo que estamos a punto de implementar, siempre deberíamos ver cómo podemos mover los bucles sobre elementos individuales de Python a algunas de las funciones de extensión NumPy o SciPy altamente optimizadas.

Sin embargo, la velocidad tiene un precio. Al usar matrices NumPy, ya no tenemos la increíble flexibilidad de las listas de Python, que pueden contener prácticamente cualquier cosa. Los arreglos NumPy siempre tienen un solo tipo de datos.


```python
>>> a = np.array([1,2,3])
>>> a.dtype
dtype('int64')
```

Si tratamos de usar elementos de diferentes tipos, NumPy hará todo lo posible para obligarlos a usar el tipo de datos común más razonable:

```python
>>> np.array([1, "stringy"]) 
array(['1', 'stringy'], dtype='|S8')
>>> np.array([1, "stringy", set([1,2,3])]) 
array([1, stringy, set([1, 2, 3])], dtype=object)
```

### Aprendiendo SciPy

SciPy ofrece una gran cantidad de algoritmos que trabajan en los arreglos de NumPy. Con SciPy se puede tratar en la manipulación de matrices, álgebra lineal, optimización, agrupación, operaciones espaciales o incluso transformación rápida de Fourier.

También se puede acceder al espacio completo de nombres de NumPy a través de SciPy. Puede verificar esto fácilmente comparando las referencias de funciones de cualquier función base; por ejemplo:

```python
>>> import scipy, numpy
>>> scipy.version.fullversion
0.11.0
>>> scipy.dot is numpy.dot
True
```

Los diversos algoritmos se agrupan en las siguientes cajas de herramientas:

| Paquete SciPy | Funcionalidad | 
|------------ | ------------|
| cluster | Clúster jerárquico (cluster.hierarchy). <br> Cuantificación vectorial / K-medias (cluster.vq). |
| constants | Constantes físicas y matemáticas.Métodos de conversión. |
| fftpack | Algoritmos de transformada discreta de Fourier. |
| integrate | Rutinas de integración. |
| interpolate | Interpolación (lineal, cúbica, etc.). |
| io | Entrada y salida de datos. |
| linalg | Rutinas de álgebra lineal utilizando los optimizados. <br> Bibliotecas BLAS y LAPACK. |
| maxentropy | Funciones para ajustar modelos de máxima entropía. |
| ndimage | Paquete de imagen n-dimensional. |
| ord | Regresión ortogonal a distancia. |
| optimize | Optimization (finding minima and roots). |
| signal | Procesamiento de la señal. |
| sparse | Matrices dispersas. |
| spatial | Estructuras de datos espaciales y algoritmos. |
| special | Funciones matemáticas especiales como Bessel o Jacobian.|
| stats | Kit de herramientas estadísticas. |


## Nuestra primera (pequeña) aplicación de aprendizaje automático.

Imaginemos que tenemos un startup web que vende el servicio de proporcionar algoritmos de aprendizaje automático a través de HTTP. Con el éxito creciente de nuestra empresa, la demanda de una mejor infraestructura también aumenta para atender con éxito todas las solicitudes web entrantes. No queremos asignar demasiados recursos ya que eso sería demasiado costoso. Por otro lado, perderemos dinero si no hemos reservado suficientes recursos para atender todas las solicitudes entrantes. La pregunta ahora es cuándo alcanzaremos el límite de nuestra infraestructura actual, que estimamos en 100,000 solicitudes por hora. Nos gustaría saber de antemano cuando tenemos que solicitar servidores adicionales en la nube para atender con éxito todas las solicitudes entrantes sin tener que pagar por las no utilizadas.


### Leyendo los datos

Se tiene un archivo llamado webtraffic.tsv (tsv porque contiene valores separados por tabulaciones). Se almacenan como el número de visitas por hora. Cada línea contiene horas consecutivas y el número de visitas web en esa hora.

Usando el genfromtxt () de SciPy, podemos leer fácilmente los datos.

```python
import scipy as sp
data = sp.genfromtxt("webtraffic.tsv", delimiter="\t")
```

Tenemos que especificar la tabulación como delimitador para que las columnas se determinen correctamente. Una comprobación rápida muestra que hemos leído correctamente en los datos.

```python
>>> print(data[:10])
[[	1.00000000e+00	2.27200000e+03]  
 [	2.00000000e+00		nan]  
 [	3.00000000e+00	1.38600000e+03]  
 [	4.00000000e+00	1.36500000e+03]  
 [	5.00000000e+00	1.48800000e+03]  
 [	6.00000000e+00	1.33700000e+03]  
 [	7.00000000e+00	1.88300000e+03]  
 [	8.00000000e+00	2.28300000e+03]  
 [	9.00000000e+00	1.33500000e+03]  
 [	1.00000000e+01	1.02500000e+03]]
>>> print(data.shape)
(743, 2)
```

Tenemos 743 puntos de datos con dos dimensiones.

### Preprocesamiento y limpieza de los datos.

Es más conveniente para SciPy separar las dimensiones en dos vectores, cada uno de tamaño 743. El primer vector, x, contendrá las horas y el otro, y, contendrá los impactos web en esa hora en particular. Esta división se realiza utilizando la notación de índice especial de SciPy, mediante la cual podemos elegir las columnas individualmente.

```python
x = data[:,0]
y = data[:,1]
```

Una advertencia es que todavía tenemos algunos valores en y que contienen valores no válidos, nan. La pregunta es, ¿qué podemos hacer con ellos? Veamos cuántas horas contienen datos no válidos.

```python
>>> print("Número de entradas inválidas:", sp.sum(sp.isnan(y)))
Número de entradas inválidas: 8
```

Nos faltan solo 8 de las 743 entradas, por lo que podemos permitirnos eliminarlas.

Podemos indexar una matriz SciPy con otra matriz. Con sp.isnan (y) devuelve una matriz de valores booleanos que indica si una entrada no es un número. 

Usando ~, lógicamente negamos esa matriz para que escojamos solo aquellos elementos de 'x' y 'y', donde y contiene números válidos.

```python
x = x[~sp.isnan(y)]
y = y[~sp.isnan(y)]
```

Para obtener una primera impresión de nuestros datos, vamos a trazar los datos en un diagrama de dispersión utilizando Matplotlib. Matplotlib contiene el paquete pyplot.


```python
import matplotlib.pyplot as plt 
plt.scatter(x,y, s=3)
plt.title("Tráfico web durante el último mes")
plt.xlabel("Tiempo")plt.ylabel("éxito/hora")
plt.xticks([w*7*24 for w in range(10)], ['Semana %i'%w for w in range(10)])
plt.autoscale(tight=True)
plt.grid()
plt.show()
```
En el gráfico resultante, podemos ver que mientras en las primeras semanas el tráfico se mantuvo más o menos igual, la última semana muestra un fuerte aumento:

![](img/image1.png)

### Elegir el modelo correcto y el algoritmo de aprendizaje

Ahora que tenemos una primera impresión de los datos, volvemos a la pregunta inicial: ¿por cuánto tiempo nuestro servidor manejará el tráfico web entrante? Para responder a esto tenemos que:

* Encontrar el modelo real detrás de los puntos de datos ruidosos.
* Usar el modelo para extrapolar hacia el futuro para encontrar el punto en el tiempo en donde nuestra infraestructura debe extenderse.

#### Antes de construir nuestro primer modelos

Cuando hablamos de modelos, puedes pensarlos como aproximaciones teóricas simplificadas de la realidad compleja. Como tal, siempre hay algo de inferioridad, también llamado error de aproximación. Este error nos guiará en la elección del modelo correcto entre la gran variedad de opciones que tenemos. Este error se calculará como la distancia al cuadrado de la predicción del modelo a los datos reales. Es decir, para una función de modelo aprendida, f, el error se calcula de la siguiente manera:

```python
def error(f, x, y):
    return sp.sum((f(x)-y)**2)
```
Los vectores 'x' y 'y' contienen los datos de estadísticas web que hemos extraído anteriormente. Se supone que el modelo entrenado toma un vector y devuelve los resultados nuevamente como un vector del mismo tamaño para que podamos usarlo para calcular la diferencia para y.


#### Comenzando con una línea recta simple

Supongamos por un segundo que el modelo subyacente es una línea recta. El desafío entonces es cómo poner mejor esa línea en el gráfico para que resulte en el error de aproximación más pequeño. La función polyfit () de SciPy hace exactamente eso. Dado los datos 'x' y 'y', y el orden deseado del polinomio (la línea recta tiene el orden 1), encuentra la función del modelo que minimiza la función del error definida anteriormente.


```python
fp1, residuals, rank, sv, rcond = sp.polyfit(x, y, 1, full=True)
```

La función polyfit () devuelve los parámetros de la función del modelo ajustado, fp1; y estableciendo full en True, también obtenemos información de fondo adicional sobre el proceso de adaptación. De ello, solo los residuos son de interés, que es exactamente el error de la aproximación.

```python
>>> print("Model parameters: %s" %	fp1)
Parámetros del modelo: [  2.59619213 989.02487106]
>>> print(residuals)
[	3.17389767e+08]
```
Esto significa que el mejor ajuste en línea recta es la siguiente función:

```
f(x) = 2.59619213 * x + 989.02487106.
```

Luego usamos poly1d () para crear una función de modelo a partir de los parámetros del modelo.

```python
>>> f1 = sp.poly1d(fp1)
>>> print(error(f1, x, y))
317389767.34
```
Se utilizo el parámetro full = True para recuperar más detalles sobre el proceso de adaptación. Normalmente, no lo necesitaríamos, en cuyo caso solo se devolverían los parámetros del modelo.

Ahora podemos usar f1 () para trazar nuestro primer modelo entrenado. Además de las instrucciones de trazado anteriores, simplemente agregamos lo siguiente:

```python
plt.scatter(x,y, s=3)
plt.title("Tráfico web durante el último mes")
plt.xlabel("Tiempo")plt.ylabel("éxito/hora")
plt.xticks([w*7*24 for w in range(10)], ['Semana %i'%w for w in range(10)])
plt.autoscale(tight=True)
plt.grid()
fx = sp.linspace(0,x[-1], 1000)
plt.plot(fx, f1(fx), linewidth=3, c="red")
plt.legend(["d=%i" % f1.order], loc="upper left")
plt.show()
```

![](img/image2.png)


Parece que las primeras cuatro semanas no están tan lejos, aunque vemos claramente que hay algo malo en nuestra suposición inicial de que el modelo subyacente es una línea recta. Además, ¿qué tan bueno o malo es en realidad el error de 317,389,767.34?

El valor absoluto del error rara vez se usa aisladamente. Sin embargo, al comparar dos modelos en competencia, podemos usar sus errores para juzgar cuál de ellos es mejor. Si bien nuestro primer modelo claramente no es el que usaríamos, tiene un propósito muy importante en el flujo de trabajo: lo usaremos como nuestra línea de base hasta que encontremos uno mejor. Cualquiera que sea el modelo que propongamos en el futuro, lo compararemos con la línea de base actual.

#### Hacia algunas cosas avanzadas

Veamos ahora un modelo más complejo, un polinomio de grado 2, para ver si mejor "entiende" nuestros datos:

```python
>>> f2p = sp.polyfit(x, y, 2)
>>> print(f2p)array([	1.05322215e-02,	-5.26545650e+00,	1.97476082e+03])
>>> f2 = sp.poly1d(f2p)
>>> print(error(f2, x, y))
179983507.878
```
El siguiente gráfico muestra el modelo que entrenamos antes (línea recta de un grado) con nuestro modelo más complejo y recientemente entrenado con dos grados (discontinua):

![](img/image2.png)

El error es 179,983,507.878, que es casi la mitad del error del modelo de línea recta. Esto es bueno; Sin embargo, viene con un precio. Ahora tenemos una función más compleja, lo que significa que tenemos un parámetro más para sintonizar dentro de polyfit (). El polinomio ajustado es el siguiente:

```
f(x) = 0.0105322215 * x**2	- 5.26545650 * x + 1974.76082
```

Entonces, si una mayor complejidad da mejores resultados, ¿por qué no aumentar la complejidad aún más? Intentémoslo para los grados 3, 10 y 100.

![](img/image3.png)

Cuanto más complejos son los datos, las curvas lo capturan y lo hacen encajar mejor. Los errores parecen contar la misma historia.

```
Error d=1: 317,389,767.339778
Error d=2: 179,983,507.878179
Error d=3: 139,350,144.031725
Error d=10: 121,942,326.363461
Error d=100: 109,318,004.475556
```

Sin embargo, al observar más de cerca las curvas ajustadas, nos preguntamos si también capturan el verdadero proceso que generó estos datos. Enmarcados de manera diferente, ¿nuestros modelos representan correctamente el comportamiento masivo subyacente de los clientes que visitan nuestro sitio web? Al observar el polinomio de grado 10 y 100, observamos un comportamiento tremendamente oscilante. Parece que los modelos están demasiado ajustados a los datos. Tanto que ahora está capturando no solo el proceso subyacente sino también el ruido. Esto se llama sobreajuste.


En este punto, tenemos las siguientes opciones: 


* Seleccionando uno de los modelos polinomiales ajustados.
* Cambiar a otra clase de modelo más complejo; splines? 
* Pensar de manera diferente sobre los datos y comenzar de nuevo.

De los cinco modelos ajustados, el modelo de primer orden claramente es demasiado simple, y los modelos de orden 10 y 100 son claramente excesivos. Solo los modelos de segundo y tercer orden parecen coincidir de alguna manera con los datos. Sin embargo, si los extrapolamos en ambas fronteras, los vemos enloqueciendo.


Cambiar a una clase más compleja también parece ser la forma incorrecta de hacerlo. ¿Qué argumentos respaldarían a qué clase? En este punto, nos damos cuenta de que probablemente no hemos entendido completamente nuestros datos.


#### Retrocediendo para seguir adelante - otra mirada a nuestros datos

Entonces, damos un paso atrás y echamos otro vistazo a los datos. Parece que hay un punto de inflexión entre las semanas 3 y 4. Entonces, separemos los datos y entrenemos dos líneas utilizando la semana 3.5 como punto de separación. Entrenamos la primera línea con los datos hasta la semana 3, y la segunda línea con los datos restantes.

```python
inflection = 3.5 * 7 * 24
xa = x[:int(inflection)]
ya = y[:int(inflection)]
xb = x[int(inflection):]
yb = y[int(inflection):]
xa = list(map(int, xa))
ya = list(map(int, ya))
xb = list(map(int, xb))
yb = list(map(int, yb))


fa = sp.poly1d(sp.polyfit(xa, ya, 1))
fb = sp.poly1d(sp.polyfit(xb, yb, 1))

fa_error = error(fa, xa, ya) 
fb_error = error(fb, xb, yb)
```

Al trazar los dos modelos para los dos rangos de datos se obtiene el siguiente gráfico:

![](img/image4.png)

Claramente, la combinación de estas dos líneas parece ajustarse mucho mejor a los datos que cualquier otra cosa que hayamos modelado anteriormente. Pero aún así, el error combinado es más alto que los polinomios de orden superior. ¿Podemos confiar en el error al final?

Preguntado de manera diferente, ¿por qué confiamos en la línea recta ajustada solo en la última semana de nuestros datos más que en cualquiera de los modelos más complejos? Es porque asumimos que capturará mejor los datos futuros. Si trazamos los modelos en el futuro, vemos lo acertados que somos (d = 1 es nuevamente nuestra línea recta inicialmente).

![](img/image5.png)

Los modelos de grado 10 y 100 no parecen esperar un futuro brillante para nuestro inicio. Intentaron tanto modelar correctamente los datos dados que son claramente inútiles para extrapolar aún más. Esto se llama sobreajuste. Por otro lado, los modelos de grado inferior no parecen ser capaces de capturar los datos correctamente. Esto se llama underfitting.

Así que juguemos de manera justa con los modelos de grado 2 y superiores y probemos cómo se comportan si los ajustamos solo a los datos de la semana pasada. Después de todo, creemos que la semana pasada dice más sobre el futuro que los datos anteriores. El resultado se puede ver en la siguiente tabla psicodélica, que muestra aún más claramente qué tan grave es el problema del sobreajuste:


![](img/image6.png)

Aún así, a juzgar por los errores de los modelos cuando se entrenó solo en los datos de la semana 3.5 y posteriores, todavía debemos elegir el más complejo.

```
Error	d=1:	22143941.107618
Error	d=2:	19768846.989176
Error	d=3:	19766452.361027
Error	d=10:	18949339.348539
Error	d=100:	16915159.603877
```


#### Entrenamiento y pruebas

Si solo tuviéramos algunos datos del futuro que podríamos usar para medir nuestros modelos, deberíamos poder juzgar nuestra elección de modelo solo en el error de aproximación resultante.

Aunque no podemos mirar hacia el futuro, podemos y debemos simular un efecto similar manteniendo una parte de nuestros datos. Eliminemos, por ejemplo, un cierto porcentaje de los datos y entrenemos en el restante. Luego usamos los datos de retención para calcular el error. Como el modelo ha sido entrenado sin conocer los datos de retención, deberíamos obtener una imagen más realista de cómo se comportará el modelo en el futuro.


```
Error	d=1:	7,917,335.831122
Error	d=2:	6,993,880.348870
Error	d=3:	7,137,471.177363
Error	d=10:	8,805,551.189738
Error	d=100:	10,877,646.621984
```

El resultado se puede ver en el siguiente gráfico:

![](img/image7.png)

Parece que finalmente tenemos un claro ganador. El modelo con grado 2 tiene el error de prueba más bajo, que es el error cuando se mide utilizando datos que el modelo no vio durante el entrenamiento. Y esto es lo que nos permite confiar en que no tendremos malas sorpresas cuando lleguen los datos futuros.









